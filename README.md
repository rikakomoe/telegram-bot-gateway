* Note: This documentation is a concept only, it may change in the future and no implementation has been made.
* Note: It is not possible to route bots you have no control of (neither made by yourself nor hosted by yourself) at the moment.

Telegram has a limit on the number of bots in a group. Are you annoyed that you can't have as many bots as you want? Are you sad because many lovely bots have to be kicked in order to make room for more important ones? Now you have a rescuer, the Telegram Bot Gateway allows multiple bots to share one account, and that shared account has all functionalities of the bots behind the gateway.


## Setting up the gateway

Create a bot account on BotFather, turn off privacy mode.

Clone the Telegram Bot Gateway (hereafter referred to as TBG) repository.

Copy 'config.sample.yaml' to 'config.yaml', edit the copied config file. This sets up the admin password and the bot token used by TBG.

Run 'bin/tbg', for all command-line options, run 'bin/tbg --help'.

By default, TBG listens on localhost:4514 for HTTP requests. It's suggested that you use a reverse proxy with a dedicated domain name and HTTPS support. Then, visit it in your browser and login with the admin password you have set just now.


## Connecting bots to the gateway

Click '+' on the dashboard.

Enter the name of the new bot.

Enter the bot token of the new bot, this can be a token acquired from BotFather, or if you don't need the bot to be used as a normal bot elsewhere, click 'Generate', this generates a virtual bot token which can only be used with this gateway.

When calling the API from the routed bot, connect to the public-facing domain of the gateway, instead of 'api.telegram.org'.


### Changing source code not desired?

If you can't or don't want to change the source code of the bot, thus need to keep using 'api.telegram.org' as the API domain, you are still covered.

In your reverse proxy configuration, add a vhost with domain 'api.telegram.org' with a self-signed SSL certificate.

On the machine the bot resides, edit '/etc/hosts' to route 'api.telegram.org' to the IP of the machine running TBG.


### Message filtering

By default, TBG forwards all text messages prefixed by a slash, all replies to messages sent by the bot and all messages mentioning TBG to all registered bots. This imitates the "privacy mode" behaviour.

To turn it off, first click the gear icon near the bot you want to configure on the dash board.

Then uncheck the box "Receive only commands, mentions and replies (like privacy mode)".

Besides privacy mode, you can configure the bot to receive only messages containing certain commands, matching certain regular expressions or making certain JavaScript code to return true. These settings apply alongside with and regardless of privacy mode setting.

To access these settings, click "Message Filters" tab in bot settings.

Message will be passed if any of these filters passes the message. But messages will first need to conform to privacy mode of it is enabled.


### Exclusive messages

Some messages may not be desirable to be received by multiple bots. For example, a message containing command '/add' may be responded by several bots, doing completely different tasks. So you can not use '/add' command with one single bot without triggering the others. But worry not, this can be worked around by setting up exclusive messages.

On the admin dashboard, click the gear icon near the bot you want to configure. Then click the "Exclusive Messages" tab.

You can set several criteria: messages containing certain commands, matching certain regular expression and making certain JavaScript code to return true. If any of these criteria is fulfilled, the message becomes 'exclusive' to this bot.

Being 'exclusive' to one bot does not change anything - all bots still receive this messages according to privacy mode setting and message filters. However, when a message becomes exclusive to multiple bots, TBG will suspend the delivery of this message and prompt the users to select one from these bots, the unselected bots will not receive the message. Note that bots don't have exclusive messages criteria fulfilled by the message will still receive the message.  


### Route bots in only certain groups

Perhaps there are only few groups that are short on bot quota, and in other groups you want to use your bot's dedicated account. That is possible with TBG, too.

On the dashboard, click the gear icon near the bot you want to configure.

Check the box "Route bot in only certain groups". Then add groups you want the bot to be routed in.

TBG will now try to use the bot token of that bot directly with Telegram bot API when interacting with groups not listed in the aforementioned list.

However, be careful that there are huge caveats when using this functionality: methods with no connection to any groups (e.g. uploadFile, createStickerSet) will be called through TBG, and file IDs are not sharable between routed and un-routed groups. If this is a problem for you, consider setting up 2 instances of your bot instead: one for normal use, one for use behind TBG.


### Private chats are supported

There's little point to use TBG in private chats, but they're supported anyway. In private chats, the message filter option "Receive only commands, mentions and replies (like privacy mode)" will be ineffective.

If you have enabled "Route bot in only certain groups", private chats of that bot will not be routed by TBG, either.


### Inline mode not supported

Merging multiple inline mode bots into one is both cumbersome and has little point. So it is not supported by TBG. If you need inline mode for your bot, use a real bot token in bot settings, and check the box "Enable inline mode". TBG will use the bot's token when calling inline mode methods.


## Bonus: Inter-bot communication

In addition to standard Telegram bot API, TBG also provides a way to communicate between bots.

TBD


## Bonus 2: High-availability bots

Here's another benefit of using TBG: you can have multiple clients pulling updates and multiple webhooks receiving same updates. This means you can have redundant instances of one bot.

TBD